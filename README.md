# booru-upgrade

![](https://img.shields.io/badge/written%20in-bash-blue)

Upgrades sample images to full resolution from xBooru servers.

## Changelog

2016-09-22 v2
- Fix an issue with handling files not present on remote server
- Fix an issue with handling sample files with leading tags in name
- Fix an issue with special characters in filenames
- [⬇️ booru-upgrade-v2.zip](dist-archive/booru-upgrade-v2.zip) *(581B)*


2016-01-16 v1
- Initial release
- [⬇️ booru-upgrade-v1.zip](dist-archive/booru-upgrade-v1.zip) *(378B)*

